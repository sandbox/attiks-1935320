<?php

/**
 * @file
 * Rules integration for invoice types.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_invoice_types_rules_action_info() {
  $actions = array();

  $actions['commerce_invoice_types_generate'] = array(
    'label' => t('Generate invoice of a certain type'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order to update'),
      ),
      'commerce_invoice_type' => array(
        'type' => 'text',
        'label' => t('Invoice type'),
        'options list' => 'commerce_invoice_types_commerce_invoice_type_options',
        'description' => t('Specifies the type of the invoice that should be created.'),
        'restriction' => 'input',
      ),
    ),
    'group' => t('Commerce Invoice'),
    'callbacks' => array(
      'execute' => 'commerce_invoice_types_rules_generate',
    ),
  );
  
  return $actions;
}

function commerce_invoice_types_commerce_invoice_type_options() {
  $options = array();
  foreach (commerce_invoice_types_invoice_types() as $type => $invoice_type) {
    $options[$type] = $invoice_type['name'];
  }
  return $options;
}

function commerce_invoice_types_rules_generate($order, $type) {
  $invoice = commerce_invoice_new($order->uid, $order->order_id, $type);
  commerce_invoice_save($invoice);
}

  
